package com.nirro01.task.filesystem;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nirro01.task.exceptions.DuplicatedNameException;
import com.nirro01.task.exceptions.IllegalNameException;
import com.nirro01.task.exceptions.IllegalSizeException;

public class FileSystemImplTest {
	
	private FileSystemImpl fileSystemImpl;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	@Before
	public void setUp() {
		fileSystemImpl = new FileSystemImpl();
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}
	@After
	public void tearDown() {
		fileSystemImpl = null;
		System.setOut(System.out);
	    System.setErr(System.err);
	}
	@Test(expected = DuplicatedNameException.class)
	public void testAddFileTwice() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile("/", "file1", 1);
		fileSystemImpl.addFile("/", "file1", 1);
	}
	@Test(expected = DuplicatedNameException.class)
	public void testAddTwoFilesWithTheSameName() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile("/", "file1", 1);
		fileSystemImpl.addFile("/", "file1", 2);
	}
	@Test(expected = DuplicatedNameException.class)
	public void testAddTwoFileandDirWithTheSameName() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile("/", "test", 1);
		fileSystemImpl.addDir("/", "test");
	}
	@Test(expected = IllegalNameException.class)
	public void testAddFileWithLongName() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile("/", "longlonglonglonglonglonglonglonglonglong", 1);
	}
	@Test(expected = IllegalNameException.class)
	public void testAddDirWithLongName() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addDir("/", "longlonglonglonglonglonglonglonglonglong");
	}
	@Test(expected = IllegalSizeException.class)
	public void testAddFileWithNegativeSize() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile("/", "test",-1);
	}
	@Test(expected = NullPointerException.class)
	public void testAddDirWithNullName() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addDir("/", null);
	}
	@Test(expected = NullPointerException.class)
	public void testAddFileWithNullName() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile("/", null,4);
	}
	@Test(expected = FileNotFoundException.class)
	public void testAddFileWithNullParent() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile(null, "test" ,4);
	}
	@Test(expected = FileNotFoundException.class)
	public void testAddDirWithNullParent() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile(null, "test" ,4);
	}
	@Test
	public void testShowFileSystem() throws DuplicatedNameException, IllegalNameException, IllegalSizeException, FileNotFoundException {
		fileSystemImpl.addFile("/", "test" ,1234);
		fileSystemImpl.showFileSystem();
		assertTrue(outContent.toString().contains("test"));
		assertTrue(outContent.toString().contains("1234"));
		outContent.reset();
		fileSystemImpl.delete("test");
		fileSystemImpl.showFileSystem();
		assertFalse(outContent.toString().contains("test"));
		assertFalse(outContent.toString().contains("1234"));
	}
}

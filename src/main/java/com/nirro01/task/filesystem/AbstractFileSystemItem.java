package com.nirro01.task.filesystem;

import java.util.Date;

import com.nirro01.task.exceptions.IllegalNameException;

public abstract class AbstractFileSystemItem {

	private Directory parent; // tree parent
	private String name;
	private Date creationDate;

	public AbstractFileSystemItem(String name, Date creationDate, Directory parent) throws IllegalNameException {
		setName(name);
		setCreationDate(creationDate);
		setParent(parent);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws IllegalNameException {
		if (name.length() > 32) {
			throw new IllegalNameException("name can't be longer than 32 characters");
		}
		this.name = name;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Directory getParent() {
		return parent;
	}

	public void setParent(Directory parent) {
		this.parent = parent;
	}
}

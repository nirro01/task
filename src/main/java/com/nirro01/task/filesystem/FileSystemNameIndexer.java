package com.nirro01.task.filesystem;

import java.util.HashMap;
import java.util.Map;

public class FileSystemNameIndexer{
	
	private Map<String,Directory> directoriesMap;
	private Map<String,File> filesMap;
	
	public FileSystemNameIndexer() {
		directoriesMap = new HashMap<String,Directory>();
		filesMap = new HashMap<String,File>();
	}
	
	public void indexFile(File file) {
		filesMap.put(file.getName(), file);
	}
	
	public void indexDir(Directory dir) {
		directoriesMap.put(dir.getName(), dir);
	}
	
	public File getFile(String fileName) {
		return filesMap.get(fileName);
	}
	
	public Directory getDir(String dirName) {
		return directoriesMap.get(dirName);
	}
	
	public Directory removeDirectory(String dirName) {
		return directoriesMap.remove(dirName);
	}
	
	public File removeFile(String fileName) {
		return filesMap.remove(fileName);
	}
}

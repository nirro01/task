package com.nirro01.task.filesystem;

import java.io.FileNotFoundException;
import java.time.Instant;
import java.util.Date;
import java.util.Stack;

import com.nirro01.task.exceptions.DuplicatedNameException;
import com.nirro01.task.exceptions.IllegalNameException;
import com.nirro01.task.exceptions.IllegalSizeException;
import com.nirro01.task.util.Pair;

/*
 * this class represent file system using a tree structure.
 * directories are tree nodes, files are tree leaves.
 * each file and folder (except root) must have a parent directory.
 * 
 * folder contains two sets of children - one for directory and one for files.
 * 
 * fileSystemNameIndexer is indexing names in order to search files and directories by name on O(1).
 * it must be updated on every change on the file system.
 */
public class FileSystemImpl implements FileSystem {
	
	private static final String ROOT = "/";
	private static final String HIRERCHY_OUTPUT_PREFIX = "    ";
	
	private Directory root;
	private FileSystemNameIndexer fileSystemNameIndexer;
	
	public FileSystemImpl() {
		try {
			root = new Directory(ROOT, Date.from(Instant.now()), null);
		} catch (IllegalNameException e) {
			// ignore, we know it will never be thrown here
		}
		fileSystemNameIndexer = new FileSystemNameIndexer();
		fileSystemNameIndexer.indexDir(root);
	}

	/*
	 * (non-Javadoc)
	 * @see com.nirro01.task.filesystem.FileSystem#addFile(java.lang.String, java.lang.String, int)
	 * 
	 */
	@Override
	public void addFile(String parentDirName, String fileName, int fileSize)
			throws IllegalNameException, IllegalSizeException, FileNotFoundException, DuplicatedNameException {
		validateNameIsUnique(fileName);
		Directory parent = getDirUsingIndex(parentDirName);
		//create file, add it to the tree and index it.
		File newFile = new File(fileName, Date.from(Instant.now()), fileSize, parent);
		parent.addFile(newFile);
		fileSystemNameIndexer.indexFile(newFile);
	}

	/*
	 * (non-Javadoc)
	 * @see com.nirro01.task.filesystem.FileSystem#addDir(java.lang.String, java.lang.String)
	 */
	@Override
	public void addDir(String parentDirName, String dirName) throws IllegalNameException, FileNotFoundException, DuplicatedNameException {
		validateNameIsUnique(dirName);
		Directory parent = getDirUsingIndex(parentDirName);
		//create directory, add it to the tree and index it.
		Directory newDir = new Directory(dirName, Date.from(Instant.now()), parent);
		parent.addDirectory(newDir);
		fileSystemNameIndexer.indexDir(newDir);	
		}

	/*
	 * (non-Javadoc)
	 * @see com.nirro01.task.filesystem.FileSystem#delete(java.lang.String)
	 * 
	 */
	@Override
	public void delete(String name) throws FileNotFoundException {
		// look for name in index and remove it
		File file = fileSystemNameIndexer.getFile(name);
		Directory dir = fileSystemNameIndexer.getDir(name);
		if(file != null) { //case 1: remove file
			removeFile(file);
		}else if (dir != null) { //case 2: remove directory
			// check user is not trying to remove root folder
			if (dir.getName().equals(ROOT)) {
				throw new IllegalNameException("cannot remove root directory");
			}
			removeDirectory(dir);
		} else { //case 3: file not found
			throw new FileNotFoundException("item " + name + " not found in the file system");			
		}
	}

	@Override
	public void showFileSystem() {
		//traverse folders using DFS algorithm,
		//the Integer is node depth for elegant output
		Stack<Pair<Directory, Integer>> fileSystemNodesStack = new Stack<Pair<Directory, Integer>>();
		//add root to the stack
		fileSystemNodesStack.push(new Pair<Directory, Integer>(root, 0));
		Pair<Directory, Integer> curr = null;
		while (!fileSystemNodesStack.isEmpty()) {
			curr = fileSystemNodesStack.pop();
			showDir(curr); //output current directory and its files
			for (Directory child : curr.getLeft().getDirectories()) {
				//push child directories to the stack
				fileSystemNodesStack.push(new Pair<Directory, Integer>(child, Integer.valueOf(curr.getRight().intValue() + 1)));
			}
		}
	}
	//output dir and its files
	private void showDir(Pair<Directory, Integer> pair) { 
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < pair.getRight(); i++) {
			sb.append(HIRERCHY_OUTPUT_PREFIX);
		}
		System.out.println(sb + pair.getLeft().toString()); //print dir
		for (File file : pair.getLeft().getFiles()) {
			System.out.println(sb + HIRERCHY_OUTPUT_PREFIX + file.toString()); //print file 
		}
	}

	private void removeDirectory(Directory dir) {
		// disconnect dir from parent
		dir.getParent().removeDirectory(dir);
		// remove indexes for directory and files inside it
		fileSystemNameIndexer.removeDirectory(dir.getName());
		dir.getFiles().forEach((f)->fileSystemNameIndexer.removeFile(f.getName()));
		// recursively remove child directories
		dir.getDirectories().forEach(d -> this.removeDirectory(d));
	}

	private void removeFile(File file) {
		//remove index
		fileSystemNameIndexer.removeFile(file.getName()); 
		//disconnect file(leaf) from parent directory
		file.getParent().removeFile(file); 
	}
	private void validateNameIsUnique(String fileName) {
		if(fileSystemNameIndexer.getFile(fileName)!= null || fileSystemNameIndexer.getDir(fileName)!= null) {
			throw new DuplicatedNameException("Name " + fileName + " is not unique in the filesystem");
		}
	}
	private Directory getDirUsingIndex(String parentDirName) throws FileNotFoundException {
		Directory parent = fileSystemNameIndexer.getDir(parentDirName);
		if(parent == null) {
			throw new FileNotFoundException("directory " + parentDirName + " not found in the file system");
		}
		return parent;
	}
}

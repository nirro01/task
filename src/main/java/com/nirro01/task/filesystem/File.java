package com.nirro01.task.filesystem;

import java.util.Date;

import com.nirro01.task.exceptions.IllegalNameException;
import com.nirro01.task.exceptions.IllegalSizeException;

public class File extends AbstractFileSystemItem {
	private int size;

	public File(String name, Date creationDate, int size, Directory parent)
			throws IllegalNameException, IllegalSizeException {
		super(name, creationDate, parent);
		setSize(size);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) throws IllegalSizeException {
		if (size < 0) {
			throw new IllegalSizeException("file size can't be negative");
		}
		this.size = size;
	}

	@Override
	public String toString() {
		return "File [Name=" + getName() + ", CreationDate=" + getCreationDate() + ", Size=" + getSize() + "]";
	}
}

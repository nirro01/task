package com.nirro01.task.filesystem;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.nirro01.task.exceptions.IllegalNameException;

public class Directory extends AbstractFileSystemItem {

	private Set<Directory> directories; // tree nodes
	private Set<File> files; // tree leaves

	public Directory(String name, Date creationDate, Directory parent) throws IllegalNameException {
		super(name, creationDate, parent);
		files = new HashSet<File>();
		directories = new HashSet<Directory>();
	}

	public Set<File> getFiles() {
		return files;
	}

	public boolean addFile(File file) {
		return files.add(file);
	}

	public boolean removeFile(File file) {
		return files.remove(file);
	}

	public Set<Directory> getDirectories() {
		return directories;
	}

	public boolean addDirectory(Directory directory) {
		return directories.add(directory);
	}

	public boolean removeDirectory(Directory directory) {
		return directories.remove(directory);
	}

	@Override
	public String toString() {
		return "Directory [Name=" + getName() + ", CreationDate=" + getCreationDate() + "]";
	}
}

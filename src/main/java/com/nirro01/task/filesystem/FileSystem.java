package com.nirro01.task.filesystem;

import java.io.FileNotFoundException;

import com.nirro01.task.exceptions.DuplicatedNameException;
import com.nirro01.task.exceptions.IllegalNameException;
import com.nirro01.task.exceptions.IllegalSizeException;

public interface FileSystem {

	/**
	 * Adds new file to the file system.
	 * 
	 * @param parentDirName
	 *            directory that will be the parent of the new file
	 * @param fileName
	 *            name of the new file
	 * @param fileSize
	 *            size of the new file
	 * 
	 * @return void
	 *
	 * @throws IllegalNameException
	 *             when fileName size longer than 32 characters
	 * @throws IllegalSizeException
	 *             when fileSize is negative
	 * @throws FileNotFoundException
	 *             when directory parentDirName does not exists
	 * @throws DuplicatedNameException
	 *             when fileName already exists in the file system
	 */
	public void addFile(String parentDirName, String fileName, int fileSize)
			throws IllegalNameException, IllegalSizeException, FileNotFoundException, DuplicatedNameException;

	/**
	 * Adds new Directory to the file system.
	 * 
	 * @param parentDirName
	 *            directory that will be the parent of the new directory
	 * @param dirName
	 *            name of the new directory
	 * 
	 * @return void
	 *
	 * @throws IllegalNameException
	 *             when dirName size longer than 32 characters
	 * @throws FileNotFoundException
	 *             when parentDirName does not exists
	 * @throws DuplicatedNameException
	 *             when fileName already exists in the file system
	 */
	public void addDir(String parentDirName, String dirName)
			throws IllegalNameException, FileNotFoundException, DuplicatedNameException;

	/**
	 * Deletes item from the file system.
	 * 
	 * @param name
	 *            name of the new item to delete. can be fileName or dirName
	 * 
	 * @return void
	 *
	 * @throws FileNotFoundException
	 *             when name does not exists in the file system
	 */
	public void delete(String name) throws FileNotFoundException;

	/**
	 * Shows all files and folder in the file system.
	 */
	public void showFileSystem();
}

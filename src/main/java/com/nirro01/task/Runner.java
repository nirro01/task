package com.nirro01.task;
import java.io.FileNotFoundException;

import com.nirro01.task.exceptions.IllegalNameException;
import com.nirro01.task.filesystem.FileSystem;
import com.nirro01.task.filesystem.FileSystemImpl;

/*
 * Program demonstration 
 */
public class Runner {

	public static void main(String[] args) throws IllegalNameException, FileNotFoundException {
		FileSystem fileSystem = new FileSystemImpl();
		fileSystem.addDir("/", "opt/");
		fileSystem.addFile("opt/", "file1.txt", 1);
		fileSystem.addFile("opt/", "file2.txt", 2);
		fileSystem.addDir("opt/", "pictures/");
		fileSystem.addFile("pictures/", "file3.jpg", 34);
		fileSystem.addDir("/", "user/");
		fileSystem.addDir("/", "bin/");
		fileSystem.addDir("/", "sys/");
		System.out.println("before deleting opt: ");
		fileSystem.showFileSystem();
		fileSystem.delete("opt/");
		System.out.println("after deleting opt: ");
		fileSystem.showFileSystem();
	}
}

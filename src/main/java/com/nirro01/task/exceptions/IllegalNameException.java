package com.nirro01.task.exceptions;

/**
 * IllegalNameException is thrown when trying to create a AbstractFileSystemItem
 * with name length longer than 32 characters
 */
public class IllegalNameException extends IllegalArgumentException {
	private static final long serialVersionUID = 1L;

	public IllegalNameException(String message) {
		super(message);
	}

	public IllegalNameException(Throwable t) {
		super(t);
	}

	public IllegalNameException(String message, Throwable t) {
		super(message, t);
	}
}

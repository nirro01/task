package com.nirro01.task.exceptions;

/**
 * IllegalSizeException is thrown when trying to create a file with negative
 * size
 */
public class IllegalSizeException extends IllegalArgumentException {
	private static final long serialVersionUID = 1L;

	public IllegalSizeException(String message) {
		super(message);
	}

	public IllegalSizeException(Throwable t) {
		super(t);
	}

	public IllegalSizeException(String message, Throwable t) {
		super(message, t);
	}
}

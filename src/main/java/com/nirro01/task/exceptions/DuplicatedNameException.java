package com.nirro01.task.exceptions;

/**
 * DuplicatedNameException is thrown when trying to add item with non unique
 * name to the file system
 */
public class DuplicatedNameException extends IllegalNameException {
	private static final long serialVersionUID = 1L;

	public DuplicatedNameException(String message) {
		super(message);
	}

	public DuplicatedNameException(Throwable t) {
		super(t);
	}

	public DuplicatedNameException(String message, Throwable t) {
		super(message, t);
	}
}
